<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $nombre="Miguel";
            $nombres=[
                "eva",
                "jose",
                "ana",
                "luis"
            ];
        ?>
        <div>
            <?= $nombre ?>
        </div>
        <div>
            <?= $nombres[0] ?>
        </div>
        <div>
            <?= $nombres[1] ?>
        </div>
        <div>
            <?= $nombres[2] ?>
        </div>
        <div>
            <?= $nombres[3] ?>
        </div>
        
        <?php
            foreach ($nombres as $k=>$v){
                echo "<div>$v</div>";
                // echo '$k-$v'; <--NO
                // echo "<div>\$v</div>"; <-- es lo mismo que arriba
                // echo '<div>' . $k . '-' . $v . '</div>'; 
                // si lo haces con comillas simples tienes que concatenar con el punto
            }
        ?>
    </body>
</html>
