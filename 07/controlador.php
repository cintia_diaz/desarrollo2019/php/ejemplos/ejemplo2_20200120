<?php
    // controlador
    session_start();
    // $_SESSION es una variable de sesión que existe siempre
    // "numeros" es un array que almacena los numeros que introduces
    // quieres saber si $_SESSION["numeros"] existe
    
    //var_dump($_GET);
    // $_GET array, variable superglobal
    // empty() es vacio o si tiene un cero
    // pregunta si $_GET está vacío:
    // $_REQUEST si envias con get o con post
    // $_GET solo sirve si has usado method="get"
    
    $accion="inicio"; // muestra el formulario
    // comprobar si he pulsado el boton enviar
    if($_GET){
        // imprimir lo que he escrito en la caja de texto (lo ultimo)
        $accion="listar"; // listar muestra los numeros
        // echo $_GET["numero"];                
        // si no existe la variable de sesion, la creo y la inicializo:
    
        if(!isset($_SESSION["numeros"])){
        // la primera vez que doy a enviar, igualo al numero que acaba de meter
            $_SESSION["numeros"]=$_GET["numero"];
        // si la variable de sesion existe añado el numero:
        } else {
        // la siguiente vez que doy a enviar, concatena:
            $_SESSION["numeros"].="," . $_GET["numero"];
        }         
        // var_dump($_SESSION);
        // el var_dump al final para imprimir la variable de sesión
    }
            /*
            if(isset($_GET["envio"])){
                var_dump($_GET);
            }
            */       
    
?>

