<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            ul {
                list-style-type: none;
            }
            a {
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <?php
         $dia = ["domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"];
        ?>
        
        <ul>
        <?php
            foreach ($dia as $valor){
        ?>
            <li>
                <a href="#"><?= $valor ?></a>
            </li>
        <?php
        }
        ?>
        </ul>
    </body>
</html>
